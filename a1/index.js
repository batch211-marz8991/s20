console.log("Hello World")

let number = Number(prompt("Enter a number"));
console.log("The number you provided is " + number);

for(number; number >=0; number--){
    if(number <= 50){
        console.log('The current value is at 50. Terminate loop');
        break;
    }
    if(number % 10 === 0){
        console.log('The number is divisible by 10. Skipping the number.');
        continue;
    }
    if(number % 5 === 0){
        console.log(number);
    }
}

let variable1 = "supercalifragilisticexpialidocious"
let variable2 = "";
for(let i = 0; i < variable1.length; i++){
    if(
        variable1[i].toLowerCase() == "a" ||
        variable1[i].toLowerCase() == "e" ||
        variable1[i].toLowerCase() == "i" ||
        variable1[i].toLowerCase() == "o" ||
        variable1[i].toLowerCase() == "u"
        ){
        console.log();
    } else {
        variable2 += variable1[i];
    }
}
console.log(variable1);
console.log(variable2);